import React from 'react';
import { Link } from 'react-router-dom';
import routes from '../../shared/constants/routes';
import { Row, Col, Accordion, Button } from 'react-bootstrap';
import { QuestionDiamond } from 'react-bootstrap-icons';

const NotFound = ({item = 'Page', errorMsg, theme}) => 
    <Row style={{...themeStyle, ...wrapperStyle}}>
        <Col xs={10} sm={10} md={8}>
            <Row style={contentStyle}>
                <p><QuestionDiamond color='var(--colorWarning)' /> Oops! {item} not found.</p> 
                <p> You can read stories <Link to={routes.HOME} 
                className={theme === 1 ? 'btn btn-dark' : ' btn btn-light'}
                style={themeStyle}
                >here</Link></p>
            </Row>
            { (errorMsg && errorMsg !== '') ?
            <Row>
                <Accordion>
                    <Accordion.Toggle as={Button} variant='link' eventKey='0'>Click here to see error message</Accordion.Toggle>
                    <Accordion.Collapse eventKey='0'><code style={codeStyle}>{errorMsg}</code></Accordion.Collapse>
                </Accordion>
            </Row>
            : ''
            }
        </Col>
    </Row>

const themeStyle = {
    backgroundColor : 'var(--colorMain)',
    color : 'var(--colorAccent)',
};

const wrapperStyle = {
    fontSize : '40px',
    fontFamily : 'Calibri, sans-serif',
    paddingTop : '10vh',
    justifyContent: 'center',
    textAlign : 'center',
};

const contentStyle = {
    display : 'grid',
    gridColumnTemplate : '',
    gridRowTemplate : '',
}

const codeStyle = {
    fontSize : '16px',
    backgroundColor : 'rgb(30,30,30)',
    padding : '30px',
}

export default NotFound;