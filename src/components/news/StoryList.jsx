import React from 'react';
import StoryListItem from './StoryListItem';
import styled from '@emotion/styled';

const StoryList = ({theme, items}) => <StoryListWrapper>
    {
        items.map( ({ id, title, img, url, comments }, i) => {
            return <StoryListItem 
                    key={id}
                    id={id}
                    url={url}
                    title={title}
                    img={img}
                    commentsCount={comments.length}
                    theme={theme}
                />
        })
    }
</StoryListWrapper>;

const StoryListWrapper = styled.div`
    display: grid;
    grid-template-columns : repeat(2, 1fr);
    grid-gap: 25px;
`;

export default StoryList;