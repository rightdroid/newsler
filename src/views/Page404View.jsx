import React from 'react';
import NotFound from '../components/common/NotFound';

const Page404 = () => <NotFound />

export default Page404;