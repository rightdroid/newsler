import React from 'react';
import { useParams, Link } from 'react-router-dom';
import { useQuery } from '@apollo/client';
import LoadingSpinner from '../components/common/LoadingSpinner';
import { Row, Col, ListGroup } from 'react-bootstrap';
import ImgWithFallback from '../components/common/ImgWithFallback';
import CommentSingle from '../components/comment/CommentSingle';
import { Link45deg, ArrowLeft } from 'react-bootstrap-icons';
import CommentAdd from '../components/comment/CommentAdd';
import queries from '../shared/constants/queries';
import routes from '../shared/constants/routes';
import NotFound from '../components/common/NotFound';

const StoryView = ({theme}) => {
    const { id } = useParams();
    const { loading, error, data } = useQuery(queries.GET_NEWS_ITEM, {
        variables: { id },
    });
    
    if (loading) return <LoadingSpinner />;
    if (error || data.newsItem == null) return <NotFound theme={theme} item='Story' errorMsg={error ? error.message : ''} />;
    
    return <Col style={themeStyle}>
        <Row>
            <Col style={{display : 'grid', gridTemplateColumns : 'auto'}}>
                <Link to={routes.HOME} 
                className={theme === 1 ? 'btn btn-dark' : ' btn btn-light'} 
                style={{...themeStyle}}>
                    <ArrowLeft />
                </Link>
                <a href={data.newsItem.url} target='_blank'
                rel="noreferrer"
                className={theme === 1 ? 'btn btn-dark' : ' btn btn-light'} 
                style={{...themeStyle,
                    gridColumn : 3,
                }}>
                    <Link45deg /> Go to Original Article
                </a>
            </Col>
        </Row>
        <Row>
            <Col style={titleStyle}>
                <h1 style={themeStyle}>{data.newsItem.title}</h1>
            </Col>
        </Row>
        <Row>
            <Col>
                <ImgWithFallback opts={
                    {src : data.newsItem.img, fluid : true, 
                    style : {display : 'block', margin : '0 auto'}}
                    } />
            </Col>
        </Row>
        <Row>
            <Col style={contentStyle}>{data.newsItem.content}</Col>
        </Row>
        <ListGroup variant='flush' id='comments' name='comments'>
            <ListGroup.Item style={themeStyle}>
                <h3 id='comments' name='comments'>Comments</h3>
            </ListGroup.Item>
            <CommentAdd theme={theme} storyId={id} />
            {data.newsItem.comments.map( (comment, i) => {
                return <CommentSingle key={comment.id} theme={theme} comment={comment} />
            })}
        </ListGroup>
    </Col>
};



const contentStyle = {
    paddingTop : '20px',
    paddingBottom : '60px',
};

const titleStyle = {
    padding : '20px 20px',
};

const themeStyle = {
    backgroundColor : 'var(--colorMain)',
    color : 'var(--colorAccent)',
};
    
export default StoryView;