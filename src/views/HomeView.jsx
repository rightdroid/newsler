import React from 'react';
import StoryList from '../components/news/StoryList';
import { useQuery } from '@apollo/client';
import LoadingSpinner from '../components/common/LoadingSpinner';
import queries from '../shared/constants/queries';


const Home = ({theme}) => {
    const { loading, error, data } = useQuery(queries.GET_NEWS_ITEMLIST);
    if (loading) return <LoadingSpinner />;
    if (error) return `Error! ${error}`;
    
    return <StoryList theme={theme} items={data.newsList.rows} />
};

export default Home;